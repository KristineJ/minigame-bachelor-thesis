# Professional programming report #

**Students involved in making the game:** Kristine Jorskogen

## Discussion ##
### Strenghts and weaknesses of languages used ###
**Java:**
Java is loved by some and hated by others. It is one of the most popular languages in software development today, so obviously it has several strengths. The following is my personal reflections on what strengths Java brings to the table, based on my experience with it during the bachelor project and from what I have read.

Java is big and widely used across the world, so as a beginner, you can easily find answers to pretty much any question by using Google and the right keywords.  I found it a huge relief to be able to find an answer within minutes instead of hours, which is often the case with C++. This is because Java for Android has specific ways of doing things, and even though there can be more than one solution to a problem, there is rarely more then a couple. So all you need to do is find the solution that works for what you need. 

Having a lot of built in libraries, you don't have to build everything from scratch. You can access the classes that are already there to get what you need, while still having some leeway when it comes to tweaking and optimizing your code directly. In Android development there is so much already there to use, so you can work efficiently even as a beginner and see progress rather fast. Assuming you are able to find what you need from Google that is. 

However, there is a downside. Java is a highly verbose language, and unfortunately this makes for more code. It's very hard to write code that is short and efficient in the same way as I am used to from C++. And you can often end up with projects that are big and very little agile. Over-engineered, some say.

Java's garbage collector is very handy, as will free up memory for any objects that are no longer referenced. Coming from C++ this allows for less work, and I appreciate that it is done for me. However, I can imagine that going from Java to C++ would be a bit more of a challenge, because you then have to do this manually - and forgetting will cause memory leaks.

Java has a lot of open source tools for pretty much whatever you need, and also a lot of open source projects online that can be accessed and used for learning. It is also a language that will run on any platform without changing it's behaviour; meaning you can write code that for example does the same on your Windows laptop as it does on your Linux server. 

There are many rules on how to write Java code. And even more for Android development. This is mostly a good thing, because it is easy to go into someone else's code and understand what they did. It's also easy to find out if you are doing it right. On the downside, you need to always keep in mind that you follow the coding conventions and rules, especially if you want something published. 

JavaDoc! Great stuff. Might be a bit of hassle learning everything at first, but generally, super useful. As long as you write the correct type of comments, you have your technical documentation by the click of a button. 

You don't have as much optimisation possibilities in Java as you do in C++. 

**XML:**
XML is widely used, and there are several other XML-based languages today (like RSS, Atom, SOAP and XHTML). My only experience with XML is through TinyXML in C++ and in Android studio. I found it to be very verbose, with massive amounts of repetition that seemed unnecessary to me, coming from a backend perspective. This also affects storage, particulary on small devices, since all this data takes up lots of memory. 

It is very useful for it's frontend use. It is not hard to learn, and understanding it was never a problem for me. In Android studio, the use of XML made my Java code less complex. It was rather easy to separate the visual, on-screen stuff from the logic. You can do almost anything from XML in Android studio, but I personally prefer to do OnClick etc from the Java classes. 

XML is a very descriptive language, and a lot of things seem redundant. I find it hard to imagine using XML for anything but visual, frontend things and data storage. 

### Process and communication systems ###
We used Slack, Facebook and Messenger as our main communication systems. Seeing as I was the only person programming, our process was quite different from the others in my class. We didn't use any of the more professional systems, though Slack is very commonly used in developer companies. Slack was the system we used together with our external and a representative from the IT company he hired to work on the backend systems of the application. But in our group, we shared progress, news, questions etc in a closed Facebook group and in our Messenger group. With our supervisor we communicated through e-mail. 

### Version control system and issue tracking ###
I used Git and Bitbucket for version control and issue tracking. This was my first experience with issue tracking, and I found the issue tracker in Bitbucket to be simple and easy to understand. Being alone, I did not see any need for using Jira. We did however use Trello for issue tracking in the group. 

For commits I used a combination of Git bash command line and the integrated Git system in Android studio. Basically, I used Android studio for all commits, but when something went wrong and I had to apply fixes, I went to the command line. Anything that wasn't just commit and push was done from the command line. 

I always worked on a development branch and merged my working code into the main branch regularly. 

### Coding conventions and commenting ###
I followed Java's conventions as well as those specific to Android developers. For comments I did JavaDoc style. 

[Coding conventions for Java](http://www.oracle.com/technetwork/java/codeconvtoc-136057.html)

[How to write Doc comments](http://www.oracle.com/technetwork/articles/java/index-137868.html)

[Code style for Android developers](https://source.android.com/source/code-style)

### Libraries ###
I did not use any libraries that wasn't already integrated in Android studio. However, if I had needed to do so, I would have just added them to my Gradle build. 

### Professionalism in my approach to software development ###
I used several tools to achieve professionalism in my approach to the development of this game. Knowing I had to analyze my code, I considered using something like Jenkins. But this seemed rather redundant, considering I was the only programmer. So I used the built-in analysis tool in Android studio that checks my code before every commit. Thus, I could go through the warnings and errors before commiting anything, and in that way make sure I only uploaded good quality code. 

I tracked my time. Initially using Toggl, during the research phase, but this didn't really work that well. I kept forgetting to put the tracker on. So instead, when I started the development, I wrote times down in a little book on my desk. This worked much better for me, and so I kept doing that. I also wrote short keywords and notes in the book for use on the report later on. 

I used semantic commits to make sure my commit log was a good, readable log of what I had been pushing to the server. This proved very useful later on, when I was searching for something specific in my previous versions. I also used issue tracking pretty much the whole way. I made issues for mostly everything, and made sure to go through often to check if I was missing something.

I re-factored code often, based on feedback from code reviews, but also from finding better options or reading up about useful, other ways of doing things.  Also, I tested my code very often. But I should have made automated tests, and not just done manual testing. The problem was that time did not allow me to sit down and learn testing as I had initially planned, because I had to focus on making the game useable and test-worthy for our test subject. I am much aware these tests are very important, and a complete must in professional Java development. 

I stuck to the Android coding style, and JavaDoc comments as much as possible. At the end I re-factored quite a bit to be up to date with the correct standards. From the comments I created Java Documentation, and I also made two UML diagrams to provide more overview of the structure of the game. Also, I tried sticking to an Android design pattern, and ended up on MVC instead of MVP or MVVM, purely to keep myself from spending too much time exploring new things. 

I always worked on a development branch, and merged it to main only when blocks of code felt complete. This way I always had two copies, in case something went wrong and everything would be lost. 

### Code reviews ###
I had four code reviews during the semester. One with Mariusz, two with Arild at Agens AS and one with Simon and one other ProProg student. From each of these reviews I took the advice and information I received and re-factored my code. This was very useful, not only for the quality of my code, but also for my learning outcome. 

Here is an example of some pretty terrible code I wrote early on, for sound effects in the game: [Soundclass early version](https://bitbucket.org/KristineJ/minigame-omhu-bachelor-thesis-kristine-jorskogen/src/b98e66ea4552ae98c4d21a155e578b8c862d1592/app/src/main/java/no/ntnu/kristinejorskogen/omhugame/SoundManager.java?fileviewer=file-view-default)
It was all static. SO MUCH STATIC! I was so stuck on reading up about Android and Java online, that I failed to consider my most basic object-oriented knowledge. This entire class was open and vulnerable, and could be referenced from just about anywhere. It would also have caused some bigger problems, if the sounds had been referenced before the setup method was called. It would have given a nullpointer exception and caused my application to crash. 
This code was re-factored to contain all final variables, where the constructor set up all the sounds upon creation of an object of the class. This way, no issues with possible memory leaks or crashes. The only public method that remains is the PlaySound. I also removed the bad, deprecated Soundpool, and used Soundpool builder instead. Here is the final version of the [Soundmanager class](https://bitbucket.org/KristineJ/minigame-omhu-bachelor-thesis-kristine-jorskogen/src/07d80da0e6d705f3e042eb25b7549206016be417/app/src/main/java/no/ntnu/kristinejorskogen/omhugame/SoundManager.java?at=master&fileviewer=file-view-default).

As for good code, I am quite pleased with my [GameObjectManager class](https://bitbucket.org/KristineJ/minigame-omhu-bachelor-thesis-kristine-jorskogen/src/07d80da0e6d705f3e042eb25b7549206016be417/app/src/main/java/no/ntnu/kristinejorskogen/omhugame/GameObjectManager.java?at=master&fileviewer=file-view-default).
It is well structured, it follows the needed coding conventions and the code is as short and effective as it can be, without lacking on the quality. I automated as much of the game object setup as possible, made sure to do a version check on the Drag and Drop method due to deprecated methods and have done my best to keep each method as small as possible. Being a prototype, it is far from perfect. Like the fact that points addition is currently just the set sum of 10, regardless. Not good! This would need to be changed in a later version. 

## Reflections ##
I have found this course to be very useful, even though I have not been able to attend many lectures. It forced me to do things in a professional manner, making me prepared for work. At several of my interviews I got asked questions about what tools I use, and I could tell them about Git, issue tracking, semantic commits etc. Also, I was able to recognize these things and how they work in Jira during my pair programming test at Agens. This I would not have been able to do without this course forcing me to work professionally on my bachelor project. I can imagine that my code would be a lot worse for it, if I had not taken this course. 

Using the tools at hand and following the general guidelines makes it better not just for me, but also for anyone who has to read or maintain my code later on. Learning these things before going into the jobmarket makes a huge difference, as every company I have talked to use these principles and tools in their work every day. I feel much more prepared for a job now, and I also felt better about my work during the semester. 

I think that professionalism in programming is extremely important for any developer. We need to follow certain rules and guidelines, so that we can work together and collaborate easier. If everyone would sit in a basement and work in their own way, with no guidelines to follow, it would be much harder to connect people and make bridges where needed. It is also important in relation to the client, as we as programmers must be able to work with the clients and allow them insight in how the work is progressing. And of course, we should deliver good quality code that is efficient and elegant, and easily maintainable. 
