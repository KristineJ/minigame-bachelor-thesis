# OMHU - The game #


### A game module developed by Kristine Jorskogen ###

For this project I had to develop a game module to be used with a bigger report tool. The usergroup for the report tool are social workers in a small, Norwegian municipality, but the game module is intended for a 41 year old test user with Downs syndrom. It should be easily adaptable for other mentally handicapped users. The task was to implement his daily routine into a game for self-reporting, while not making too big changes from the pen, paper and physical rewards he gets today. The game is a digital version of the routine he already has, and it offers more motivation for the user, as well as an easier and more efficient approval process for his social worker.

The game was developed in Android studio, and it is part of my bachelor thesis in game programming at NTNU Gj�vik, spring 2017.  

See video here: https://youtu.be/2NVng5P2q84