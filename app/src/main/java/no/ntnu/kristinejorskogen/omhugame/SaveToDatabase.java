/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Handles saving to the database
 * @author Kristine Jorskogen
 */
public class SaveToDatabase {

    private final Context mContext;
    private final ImageButton mApproveButton;
    private final Intent mIntent;
    private final int mPoints;

    /**
     * Creates a SaveToDatabase object
     * @param context Context from class that creates object
     * @param view View to be used
     * @param intent Intent from activity
     * @param points Points collected
     */
    SaveToDatabase (Context context, View view, Intent intent, int points) {
        mContext = context;
        mIntent = intent;
        mPoints = points;
        mApproveButton = (ImageButton)view.findViewById(R.id.approveButton);
        mApproveButton.setOnClickListener(approveButtonListener);
    }

    final ImageButton.OnClickListener approveButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            createPopupWindow(view);
        }
    };

    /**
     * Creates a popup window and it's content
     * @param view View from activity
     */
    private void createPopupWindow(View view) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext
                .getSystemService(LAYOUT_INFLATER_SERVICE);

        View popupView = layoutInflater.inflate(R.layout.item_approve_report, null);

        // Get list of activities
        final ArrayList<String> list = mIntent.getStringArrayListExtra("list");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mContext,
                R.layout.item_row_layout_approve, R.id.activityTextView, list);

        ListView listView = (ListView) popupView.findViewById(R.id.approveList);
        listView.setAdapter(arrayAdapter);

        final PopupWindow popupWindow = new PopupWindow(popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.setFocusable(true);
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        setUpSaveButton(popupView, list);
        
    }

    /**
     * Sets up the savebutton
     * @param popupView View for popup window
     * @param list List of activities
     */
     private void setUpSaveButton (View popupView, final ArrayList<String> list) {
         final EditText editTextName = (EditText) popupView.findViewById(R.id.editTextName);
         final EditText editTextPassword = (EditText) popupView.findViewById(R.id.editTextPassword);

         ImageButton saveActivitiesButton = (ImageButton) popupView.findViewById(R.id.saveListButton);
         saveActivitiesButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 final String name = editTextName.getText().toString();
                 final String password = editTextPassword.getText().toString();
                 DatabaseOperations databaseOperations = new DatabaseOperations(mContext);

                 /* Save to database if name longer than one char and password not null,
                 then go to startscreen */
                 if (name.length() > 1) {
                     if (password != null) {
                         for (int i = 0; i < list.size(); i++) {
                             databaseOperations.insertUserInfo(databaseOperations, list.get(i).toString(), name, 10);
                         }
                         mContext.startActivity(new Intent(mContext, StartscreenActivity.class));
                     } else {
                         Toast.makeText(mContext, "Feil passord. Vennligst prøv igjen.", Toast.LENGTH_SHORT).show();
                     }
                 } else {
                     Toast.makeText(mContext, "Ugyldig navn. Navn må ha minst 2 bokstaver.", Toast.LENGTH_SHORT).show();
                 }
             }
         });
     }
}
