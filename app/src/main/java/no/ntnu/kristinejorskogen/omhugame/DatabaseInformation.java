/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.provider.BaseColumns;

/**
 * Handles the database tables
 * @author Kristine Jorskogen
 */
public class DatabaseInformation {

    /**
     * Sets the database table for user information
     */
    public static abstract class UserInfoTable implements BaseColumns {
        public static final String TABLE_NAME = "user_info";
        public static final String DATABASE_NAME = "OMHU";
        public static final String COLUMN_ID = "_id";
        public static final String ACTIVITY = "activity";
        public static final String DATE = "date";
        public static final String CARETAKER = "caretaker";
        public static final String POINTS = "points";
    }
}
