/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Handles background music
 * @author Kristine Jorskogen
 */
public class MusicService extends Service {

    private MediaPlayer mMediaPlayer;
    private final IBinder mBinder = new Binder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Called when service is created
     */
    public void onCreate()
    {
        mMediaPlayer = MediaPlayer.create(this, R.raw.littlerobotsoundfactory__someday);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.setVolume(1.0f, 1.0f);
        mMediaPlayer.start();
    }

    /**
     * Called when service is destroyed
     */
    public void onDestroy()
    {
        mMediaPlayer.stop();
        mMediaPlayer.release();
    }

}
