/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

/**
 * Helper class for database to provide information
 * @author Kristine Jorskogen
 */
public class DataProvider {

    private String mActivity;
    private String mDate;
    private String mCaretaker;
    private String mPoints;

    /**
     * Creates a DataProvider object
     * @param activity Describes the activity completed
     * @param date Current date
     * @param caretaker Caretakers name
     * @param points Amount of points collected
     */
    DataProvider(String activity, String date, String caretaker, String points) {

        this.mActivity = activity;
        this.mDate = date;
        this.mCaretaker = caretaker;
        this.mPoints = points;
    }

    public String getmActivity() {
        return mActivity;
    }

    public String getmDate() {
        return mDate;
    }

    public String getmCaretaker() {
        return mCaretaker;
    }

    public String getmPoints() {
        return mPoints;
    }
}
