/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/**
 * Activity for the Startscreen
 * @author Kristine Jorskogen
 */
public class StartscreenActivity extends AppCompatActivity {

    private boolean mBound;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startscreen);
        mContext = this;

        // Set orientation to landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Set background transparency
        View view = findViewById(R.id.activity_startscreen);
        view.getBackground().setAlpha(230);

        StartscreenManager startscreenManager = new StartscreenManager(mContext, view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Bind to music service
        Intent intent = new Intent(mContext, MusicService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the music service
        if (mBound) {
            unbindService(connection);
            mBound = false;
        }

    }

    //Connection used for service and it's binder
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}


