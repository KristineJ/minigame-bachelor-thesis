/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Handles all database operations
 * @author Kristine Jorskogen
 */
public class DatabaseOperations extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION = 3;

    /**
     * Creates a new DatabaseOperations object
     * @param context Context from class that creates object
     */
    DatabaseOperations(Context context) {
        super(context, DatabaseInformation.UserInfoTable.DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(
                "CREATE TABLE " + DatabaseInformation.UserInfoTable.TABLE_NAME + "("
                        + DatabaseInformation.UserInfoTable.COLUMN_ID + " INTEGER PRIMARY KEY, "
                        + DatabaseInformation.UserInfoTable.ACTIVITY + " TEXT, "
                        + DatabaseInformation.UserInfoTable.DATE + " TEXT, "
                        + DatabaseInformation.UserInfoTable.CARETAKER + " TEXT, "
                        + DatabaseInformation.UserInfoTable.POINTS + " INTEGER)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO: Make a better onUpgrade that doesn't just destroy the old database right away
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInformation.UserInfoTable.TABLE_NAME);
        onCreate(db);
    }

    /**
     * Inserts data into the database
     * @param dbo DatabaseOperations object
     * @param activity Describes the activity completed
     * @param caretaker Caretakers name
     * @param points Amount of points collected
     */
    public void insertUserInfo(DatabaseOperations dbo, String activity, String caretaker, int points) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = sdf.format(new Date());

        SQLiteDatabase SQ = dbo.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put(DatabaseInformation.UserInfoTable.ACTIVITY, activity);
        content.put(DatabaseInformation.UserInfoTable.DATE, date);
        content.put(DatabaseInformation.UserInfoTable.CARETAKER, caretaker);
        content.put(DatabaseInformation.UserInfoTable.POINTS, points);
        SQ.insert(DatabaseInformation.UserInfoTable.TABLE_NAME, null, content);
    }

    /**
     * Gets information from database
     * @param dbo DatabaseOperations object
     * @return SQLite database object with the information stored in database
     */
    public Cursor getDbInfo(DatabaseOperations dbo) {
        SQLiteDatabase SQ = dbo.getReadableDatabase();
        return SQ.rawQuery("SELECT * FROM " + DatabaseInformation.UserInfoTable.TABLE_NAME, null);
    }
}
