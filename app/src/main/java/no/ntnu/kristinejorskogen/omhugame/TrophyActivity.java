/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Activity for the overview of trophies and points
 * @author Kristine Jorskogen
 */
public class TrophyActivity extends AppCompatActivity {
    private Context mContext;
    private boolean mBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trophy);
        mContext = this;

        // Set orientation to landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        TextView pointsText = (TextView)findViewById(R.id.pointsSummedTextView);
        TextView headerText = (TextView)findViewById(R.id.headerTextView);

        // Set custom font
        Typeface customFont = Typeface.createFromAsset(getAssets(),  "fonts/cartoon_bold.ttf");
        pointsText.setTypeface(customFont);
        headerText.setTypeface(customFont);

        DatabaseOperations mDatabaseOperations = new DatabaseOperations(mContext);
        SQLiteDatabase mSQLiteDatabase = mDatabaseOperations.getReadableDatabase();
        Cursor mCursor = mDatabaseOperations.getDbInfo(mDatabaseOperations);
        int totalPoints = 0;

        //Add 10 points to the total for every line in the database
        //TODO: Add a method for when a prize is earned and points must be reset
        if (mCursor.moveToFirst()) {
            do {
                totalPoints += 10;
            } while (mCursor.moveToNext());
        }
        pointsText.setText(getString(R.string.totalpoeng) + totalPoints);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Bind to music service
        Intent intent = new Intent(mContext, MusicService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the music service
        if (mBound) {
            unbindService(connection);
            mBound = false;
        }

    }

    //Connection used for service and it's binder
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}
