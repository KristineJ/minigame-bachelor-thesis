/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.SoundPool;

/**
 * Handles all sounds and their methods
 * @author Kristine Jorskogen
 */
public class SoundManager {

    private final SoundPool mGameSounds;
    public final int mCorrectDrop;
    public final int mWrongDrop;
    public final int mHighscore;

    /**
     * Creates a SoundManager object
     * @param context Context from class that creates object
     */
    SoundManager (Context context) {
        //Set up and initialize game sounds
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();

        mGameSounds = new SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(audioAttributes)
                .build();

        mCorrectDrop = mGameSounds.load(context, R.raw.gotitem2, 1);
        mWrongDrop = mGameSounds.load(context, R.raw.lostitem, 1);
        mHighscore = mGameSounds.load(context, R.raw.highscore1, 1);
    }

    /**
     * Plays the requested sound
     * @param sound Which sound to be played
     */
    public void playSound (int sound) {
        mGameSounds.play(sound, 1.0f, 1.0f, 0, 0, 1.5f);
    }
}
