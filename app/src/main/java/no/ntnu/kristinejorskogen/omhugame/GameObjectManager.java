/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Handles all operations on game objects
 * @author Kristine Jorskogen
 */
public class GameObjectManager {
    private final Context mContext;
    private final SoundManager mSoundManager;
    private final ImageButton mSaveButton;

    private String mTouchViewTag;
    private String mDragViewTag;
    private View mTouchView;
    private int mPoints;
    private int mDropCounter;

    private final int[] mImageViewIdArray = {
            R.id.imageView1, R.id.imageView2, R.id.imageView3, R.id.imageView4,
            R.id.imageView5, R.id.imageView6, R.id.imageView7, R.id.imageView8,
            R.id.imageView9, R.id.imageView10, R.id.imageView11, R.id.imageView12
            };
    private final int[] mImageViewAlphaIdArray = {
            R.id.imageView1alpha, R.id.imageView2alpha, R.id.imageView3alpha, R.id.imageView4alpha,
            R.id.imageView5alpha, R.id.imageView6alpha, R.id.imageView7alpha, R.id.imageView8alpha,
            R.id.imageView9alpha, R.id.imageView10alpha, R.id.imageView11alpha, R.id.imageView12alpha
    };

    private final int mTotalGameObjects = mImageViewIdArray.length;

    private ArrayList<String> mList;

    /**
     * Creates a GameObjectManager object
     * @param context Context from class that creates object
     */
    GameObjectManager (Context context, View view) {
        mContext = context;
        mPoints = 0;
        mDropCounter = 0;
        mList = new ArrayList<>();
        mSoundManager = new SoundManager(context);
        setUpImageViews(view);

        mSaveButton = (ImageButton) view.findViewById(R.id.saveButton);
        mSaveButton.setOnTouchListener(savebuttonTouchListener);
    }

    private final ImageButton.OnTouchListener savebuttonTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (mPoints != 0) {
                mSaveButton.setEnabled(false);
                goToFeedback();
                return true;
            } else {
                Toast.makeText(mContext, R.string.toast_message_save, Toast.LENGTH_LONG).show();
            }

            return false;
        }
    };

    private final ImageView.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mTouchView = view;
            mTouchViewTag = String.valueOf(view.getTag());

            // Switch that handles touch events
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                            view);
                    if (Build.VERSION.SDK_INT >= 24) {
                        view.startDragAndDrop(data, shadowBuilder, view, 0);
                    } else {
                        view.startDrag(data, shadowBuilder, view, 0);
                    }
                    break;
                default:
                    return false;
            }
            return true;
        }
    };

     private final ImageView.OnDragListener dragListener = new View.OnDragListener() {
        @Override
        public boolean onDrag(View view, DragEvent dragEvent) {
            // Switch that handles drag events
            switch (dragEvent.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    break;
                case DragEvent.ACTION_DROP:
                    Boolean matchingViews = checkMatchingViews(view);
                    if (matchingViews) {
                        break;
                    } else {
                        mSoundManager.playSound(mSoundManager.mWrongDrop);
                        return false;
                    }
                default:
                    return false;
            }
            return true;
        }
    };

    /**
     * Checks if game objects matches and goes to feedback upon completion of game
     * @param view The view from the current activity
     * Returns true if image is dropped in correct place
     */
    private Boolean checkMatchingViews(View view) {
        Boolean matchingViews;
        mDragViewTag = String.valueOf(view.getTag());
        matchingViews = mDragViewTag.equals(mTouchViewTag);

        /* If the image dragged matches the image on drop location,
         make drop location full alpha, play sound and add to points */
        if (matchingViews) {
            mTouchView.setVisibility(View.INVISIBLE);
            view.setAlpha(1.0F);
            mSoundManager.playSound(mSoundManager.mCorrectDrop);
            mPoints += 10;
            mDropCounter++;
            mList.add(mList.size(), mDragViewTag);

            if (mDropCounter == mTotalGameObjects) {
                goToFeedback();
            }
            return true;
        }
        return false;
    }

    /**
     * Sets up the game objects with touch or drag mListeners
     * @param view Content view from activity that calls method
     */
    private void setUpImageViews(View view) {
        for (int i = 0; i < mTotalGameObjects; i++) {
            ImageView imageView = (ImageView) view.findViewById(mImageViewIdArray[i]);
            imageView.setOnTouchListener(touchListener);

            ImageView imageViewAlpha = (ImageView) view.findViewById(mImageViewAlphaIdArray[i]);
            imageViewAlpha.setOnDragListener(dragListener);
        }
    }

    /**
     * Handles events when the game activity resumes
     */
    public void onResume() {
        mSaveButton.setEnabled(true);
    }

    /**
     * Starts feedback activity
     */
    private void goToFeedback() {
        Intent intent = new Intent(mContext, FeedbackActivity.class);
        intent.putExtra("points", mPoints);
        intent.putStringArrayListExtra("list", mList);
        mContext.startActivity(intent);
        mSoundManager.playSound(mSoundManager.mHighscore);
    }
}



