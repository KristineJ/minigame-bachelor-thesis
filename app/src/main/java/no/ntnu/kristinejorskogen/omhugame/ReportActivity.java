/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

/**
 * Activity for the report 
 * @author Kristine Jorskogen
 */
public class ReportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Context context = this;

        // Set orientation to landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //Set listview, adapter and database content for showing database info on screen
        ListView listView = (ListView) findViewById(R.id.reportList);
        DisplayAdapter displayAdapter =  new DisplayAdapter(context, R.layout.item_row_layout_report);
        listView.setAdapter(displayAdapter);
        DatabaseOperations databaseOperations = new DatabaseOperations(context);
        SQLiteDatabase SQLiteDatabase = databaseOperations.getReadableDatabase();
        Cursor cursor = databaseOperations.getDbInfo(databaseOperations);

        //If there is something on the list, show info on screen
        if (cursor.moveToFirst()) {
            do {
                String activity, date, caretaker, points;
                activity = cursor.getString(1);
                date = cursor.getString(2);
                caretaker = cursor.getString(3);
                points = cursor.getString(4);

                DataProvider dataProvider = new DataProvider(activity, date, caretaker, points);
                displayAdapter.add(dataProvider);
            } while (cursor.moveToNext());
        }

    }
}
