/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for database to display information in a view
 * @author Kristine Jorskogen
 */
public class DisplayAdapter extends ArrayAdapter {

    private List mList = new ArrayList();

    /**
     * Creates a DisplayAdapter object
     * @param context Context from class that creates object
     * @param resource The layout ID to be used
     */
    DisplayAdapter(Context context, int resource) {
        super(context, resource);
    }

    /**
     * Handles layout views
     */
    static class LayoutHandler {
        TextView mActivity;
        TextView mCaretaker;
        TextView mDate;
    }

    @Override
    public void add(Object object) {
        super.add(object);
        mList.add(object);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        LayoutHandler layoutHandler;
        
        // Sets rows in layout
        if(row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.item_row_layout_report, parent, false);
            layoutHandler = new LayoutHandler();
            layoutHandler.mActivity = (TextView) row.findViewById(R.id.activityTextView);
            layoutHandler.mDate = (TextView) row.findViewById(R.id.dateTextView);
            layoutHandler.mCaretaker = (TextView) row.findViewById(R.id.careTakerTextView);
            row.setTag(layoutHandler);
        } else {
            layoutHandler = (LayoutHandler) row.getTag();
        }

        // Sets text for the views
        DataProvider dataProvider = (DataProvider)this.getItem(position);
        layoutHandler.mActivity.setText(dataProvider.getmActivity());
        layoutHandler.mDate.setText(dataProvider.getmDate());
        layoutHandler.mCaretaker.setText(dataProvider.getmCaretaker());
        
        return row;
    }
}
