/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;

/**
 * Handles objects on the Startscreen
 * @author Kristine Jorskogen
 */
public class StartscreenManager {

    private final Context mContext;

    /**
     * Creates new StartscreenManager object
     * @param context Context from class that creates object
     * @param view Current view
     */
    StartscreenManager(Context context, View view) {
        mContext = context;
        setupButtons(view);
    }

    /**
     * Sets up the buttons and menu for the btn_start screen
     * @param view Content view from activity that calls method
     */
    private void setupButtons (View view) {

        final View menuView = view.findViewById(R.id.menu);
        menuView.setVisibility(View.GONE);

        final ImageButton startButton = (ImageButton) view.findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when button is clicked
             * @param v Current view
             */
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, GameActivity.class));
            }
        });

        final ImageButton menuListButton = (ImageButton) view.findViewById(R.id.menuListButton);
        menuListButton.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when button is clicked
             * @param v Current view
             */
            @Override
            public void onClick(View v) {

                // If menu already showing, hide - otherwise show
                if (menuView.getVisibility() == View.VISIBLE) {
                    menuView.setVisibility(View.GONE);
                }
                else {
                    menuView.setVisibility(View.VISIBLE);
                }
            }
        });

        final ImageButton reportButton = (ImageButton) view.findViewById(R.id.reportButton);
        reportButton.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when button is clicked
             * @param v Current view
             */
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, ReportActivity.class));
            }
        });

        final ImageButton overviewButton = (ImageButton) view.findViewById(R.id.overviewButton);
        overviewButton.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when button is clicked
             * @param v Current view
             */
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, TrophyActivity.class));
            }
        });
    }

}
