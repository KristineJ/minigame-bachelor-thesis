/*
 * Copyright (c) 2017 Kristine Jorskogen
 */

package no.ntnu.kristinejorskogen.omhugame;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Activity for the feedback screen
 * @author Kristine Jorskogen
 */
public class FeedbackActivity extends AppCompatActivity {

    private Context mContext;
    private boolean mBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        mContext = this;

        // Set orientation to landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        TextView feedbackText = (TextView)findViewById(R.id.feedbackTextView);
        TextView pointsText = (TextView)findViewById(R.id.pointsTextView);

        // Set custom font
        Typeface customFont = Typeface.createFromAsset(getAssets(),  "fonts/cartoon_bold.ttf");
        feedbackText.setTypeface(customFont);
        pointsText.setTypeface(customFont);

        // Get and print points earned
        final Intent intent = getIntent();
        final int points = intent.getIntExtra("points", 0);
        pointsText.setText("Du har samlet " + points + " poeng");

        final ImageButton approveButton = (ImageButton)findViewById(R.id.approveButton);
        approveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveToDatabase saveToDatabase = new SaveToDatabase(mContext, v, intent, points);
            }
        });

        final ImageButton cancelButton = (ImageButton)findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, StartscreenActivity.class));
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        //Bind to music service
        Intent intent = new Intent(mContext, MusicService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the music service
        if (mBound) {
            unbindService(connection);
            mBound = false;
        }

    }

    //Connection used for service and it's binder
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}
